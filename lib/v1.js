window.Q = function () {

    var console = window.console ? window.console : {log: function () {}};

    return function (out, done) {

        var qIndex;
        var questions = [];//questions are presented in FIFO
        var answers = {};//lookup table such that we can ask follow up Q based on Yes No, etc

        var abort = function () {
            qIndex = questions.length + 1;
        };

        var currentQuestion = function () {
            return questions[ qIndex ];
        };

        //used in design phase to modify the question behavior
        var lastQuestionInQue = function () {
            return questions[questions.length-1];
        };

        // replace HTML to show Q shape
        var presentQuestion = function (q) {
            out.innerHTML = q.htmlform;
        };

        //inspect radio button and return value if checked
        var getSingleChoiceAnswer = function ( name ) {
            var radios = document.getElementsByName( name );
            var i, answer, max = radios.length;
            for (i=0; i<max; i+=1) {
                if (radios[i].checked) {
                    answer = radios[i].value;
                }
            }
            return answer;
        };

        // inspect check boxes and return values if checked
        var getMultiChoiceAnswer = function ( name ) {
            var checkboxes = document.getElementsByName( name );
            var i, answers = [], max = checkboxes.length;
            for (i=0; i<max; i+=1) {
                if (checkboxes[i].checked) {
                    answers.push( checkboxes[i].value );
                }
            }
            return answers;
        };

        // inspect text containers and return txt inputs
        var getTxtAnswer = function ( name ) {
            var txtContainers = document.getElementsByName( name );
            var i, answers = [], max = txtContainers.length;
            for (i=0; i<max; i+=1) {
                answers.push( txtContainers[i].value );
            }
            return answers.join("\n");
        };

        //note to self
        //to implement required field such as email
        //have the q.parseAnswer throw an exception
        var processAnswer = function (form) {

            var q = currentQuestion(); console.log("proc answer for "+q.qry);
            var answer = q.parseAnswer( form ); console.log("answer:"+answer);
            if (answer === undefined || answer == null) {
                //radio is not selected
                alert("Please provide an answer.");
            } else {
                //allowing blank "" as answer which is an acceptable answer to rhetorical q
                q.answer = answer;
                if (q.after(answer)) {
                    askNextQuestion();
                } else {
                    //stops the questionnaire if after returns false
                    console.log("stop per unmet after condition");
                    done(questions);
                }
            }
        };

        // as long as the qIndex is within the range
        // if before condition is NOT met, we skip to the next Q
        var askNextQuestion = function () {
            qIndex += 1;
            if (qIndex < questions.length) {
                var q = currentQuestion();
                if (q.before()) {
                    q.run()
                } else {
                    askNextQuestion()
                }
            } else {
                done(questions);
            }
        };

        // create and/or augment Q
        // this is where you edit to accomodate a new Q "shape"
        var designer = function (q, qry) {

            q = q ? q : {};// 

            q.qnum = "q-" + (questions.length + 1);
            q.qry = qry;

            q.htmlform = "<form onsubmit='return Q.processAnswer(event);' id='form-"+q.qnum+"' >";
            q.htmlform += q.qry;
            q.htmlform += "<br>";
            q.htmlform += "<input type=submit value='Next'>";
            q.htmlform += "</form>";
            
            q.parseAnswer = function (form) {
                return "";
            };
            // if there is q, it should have "before" defined
            q.before = q.before ? q.before : function () { 
                // this will allow this.run()
                return true;
            };

            q.run = q.run ? q.run : function () {
                presentQuestion( this );
            };

            q.after = q.after ? q.after : function ( answer ) {
                return true; // otherwise next q is not run
            };

            // it's possible that this is a final step of design for this Q
            questions.push( q );


            // return modifiers which modifies last inserted question
            return {
                using: function () {
                    var q = lastQuestionInQue();

                    var shape = arguments[0]; console.log(shape);
                    var parts = [];
                    for (var i = 1; i < arguments.length; i++) {
                        parts.push( arguments[i] )
                    }

                    var shapes ={
                        "radio": function (q, parts) {
                            var s = "<form onsubmit='return Q.processAnswer(event);' id='form-"+q.qnum+"' >";
                            s += q.qry;
                            s += "<br>";

                            for (var i=0;i<parts.length;i+=1) {
                                s += "<input type=radio name='"+q.qnum+"' value='"+parts[i]+"'> "+parts[i]+"<br>";
                            }

                            s += "<input type=submit value='Next'>";
                            s += "</form>";
                            return s;
                        },
                        "checkbox": function (q, parts) {
                            var s = "<form onsubmit='return Q.processAnswer(event);' id='form-"+q.qnum+"' >";
                            s += q.qry;
                            s += "<br>";

                            for (var i=0;i<parts.length;i+=1) {
                                s += "<input type=checkbox name='"+q.qnum+"' value='"+parts[i]+"'> "+parts[i]+" <br>";
                            }

                            s += "<input type=submit value='Next'>";
                            s += "</form>";
                            return s;
                        },
                        "textarea": function (q, parts) {
                            var s = "<form onsubmit='return Q.processAnswer(event);' id='form-"+q.qnum+"' >";
                            s += q.qry;
                            s += "<br>";

                            s += "<textarea name='"+q.qnum+"' rows=5 cols=80 autofocus></textarea> <br>";

                            s += "<input type=submit value='Next'>";
                            s += "</form>";
                            return s;
                        },
                        "text":  function (q, parts) {

                            var s = "<form onsubmit='return Q.processAnswer(event);' id='form-"+q.qnum+"' >";
                            s += q.qry;
                            s += "<br>";
                            s += "<input type=text name='"+q.qnum+"' autofocus> <br>";
                            s += "<input type=submit value='Next'>";
                            s += "</form>";
                            return s;
                        }
                    };
                    q.htmlform = shapes[shape](q, parts); console.log(q.htmlform);

                    var parsers = {
                        "radio": function (q, parts) {
                            return function (form) {
                                return getSingleChoiceAnswer(q.qnum);
                            };
                        },
                        "checkbox": function (q, parts) {
                            return function (form) {
                                return getMultiChoiceAnswer(q.qnum);
                            };
                        },
                        "textarea": function (q, parts) {
                            return function (form) {
                                return getTxtAnswer(q.qnum);
                            };
                        },
                        "text":  function (q, parts) {
                            return function (form) {
                                return getTxtAnswer(q.qnum);
                            };
                        }
                    };
                    q.parseAnswer = parsers[shape](q, parts);


                    return this;
                },
                saveAs: function (name) {
                    var q = lastQuestionInQue();
                    // we record the answer to a lookup table
                    q.after = function (answer) {
                        answers[ name ] = answer;
                        return true;
                    };

                    return null;
                },
                stop: function () {
                    var q = lastQuestionInQue();
                    q.after = function ( answer ) {
                        // this stops asking the next queston
                        console.log("stopper!")
                        return false;
                    };
                    return null;
                }
            };
        };

        return {
          ask: function (qry) {
            return designer(false, qry);
          },
          answerTo: function (name) {
            return {
                equals: function (val) {
                    var q = {
                        before: function () {
                            console.log("checking answer: "+answers[name]+" vs "+val);
                            return answers[name] === val;
                        }
                    };
                    return {
                        ask: function (qry) {
                            return designer(q, qry);
                        }
                    };
                },
                includes: function () {
                    var _args = [];
                    for (var i=0; i<arguments.length; i+=1) {
                        _args.push( arguments[i] );
                    }
                    var q = {
                        before: function () {
                            console.log("checking answer: "+answers[name]+" vs "+_args);
                            for (var i=0; i<_args.length; i+=1) {
                                if (answers[name].indexOf( _args[i] ) < 0) {
                                    console.log(_args[i]+" is NOT in "+answers[name]);
                                    return false;
                                } else {
                                    console.log(_args[i]+" is in "+answers[name]);
                                }
                            }
                            return true;
                        }
                    };
                    return {
                        ask: function (qry) {
                            return designer(q, qry);
                        }
                    };
                }
            };
          },
          processAnswer: function (evt) {
                evt.preventDefault();
                var inputs = evt.target.getElementsByTagName("input");
                var length = inputs.length;
                for (var i=0; i<length; i++) {
                    if ("submit" === inputs[i].type) {
                        inputs[i].disabled = true;
                    }
                }
                setTimeout(function () {
                    processAnswer(evt.target);
                }, 50);
                return false;//to stop posting the form
          },
          start: function () {
            qIndex = -1;// critical per how askNextQuestion uses qIndex
            return askNextQuestion(); 
          }
        };

    }
};