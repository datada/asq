parse-save-URL.sh (ctr-z to stop)

```sh
while [ 1 ]
do
  ec2din $(cat ins.id) > tmp
  cat tmp | racket rkts/parse-url.rkt > ins.url
  cat ins.url
  sleep 5
done
```

rkt/parse-url.rkt:

```rkt
(define (in->url in)
   (for ([line (in-lines in)])
      (for ([word (regexp-split #rx"\t+" line)])
         (if (and (string? word)
                  (regexp-match #rx"amazonaws.com$" word))
             (display word)
             (void)))))
          
(in->url (current-input-port))
```