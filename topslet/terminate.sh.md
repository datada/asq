terminate.sh

```sh
ec2-terminate-instances $(cat ins.id)
while [ 1 ]
do
  sleep 3
  echo "++++++++++++++++++++++++++++++++++++++++++"
  ec2din
done
```