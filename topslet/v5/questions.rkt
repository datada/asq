#lang racket

;(require web-server/servlet)
(require web-server/http/bindings)

(provide (all-defined-out))

;question is constructed at the run time 
;gui constructors are invoked with name providing form and parser
(struct question (name title form parser))

;form spits out xexpr
;parser gets value from request bindings
(struct gui (form parser))

(define (make-q nom words gui-builder)
  (let* ([title (apply string-append words)]
         [a-gui (gui-builder nom)])

    (question nom
              title
              `(fieldset (legend "Question")
                (p ,title)
                ,@((gui-form a-gui)))
              (gui-parser a-gui)
              )))

(define (make-single-extractor nom)
  (λ (binds)
    (extract-binding/single (string->symbol (symbol->string nom))
                            binds)))

(define (make-multiple-extractor nom)
  (λ (binds)
    (extract-bindings (string->symbol (symbol->string nom))
                      binds)))

;; GUI consructors

(define free
  (λ (nom)
    (gui (λ ()
           (list
            `(input ([type "text"]
                     [name ,(symbol->string nom)]
                     [size "40"]))))
         (make-single-extractor nom))))

(define (pull-down-input val)
  `(option ([value ,val])
           ,val))

(define pull-down
  (λ args
    (λ (nom)
      (gui (λ ()
             (list `(select ([name ,(symbol->string nom)])
                            ,@(for/list ([i (in-naturals)]
                                         [arg args])
                                        (pull-down-input arg)))))
           (make-multiple-extractor nom)))))

(define (radio-input nom val)
  `(input ([type "radio"]
           [name ,nom]
           [value ,val])
          ,val))

(define radio
  (λ args
    (λ (nom)
      (gui (λ ()
             (for/list ([i (in-naturals)]
                        [arg args])
                       (radio-input (symbol->string nom) arg)))
           (make-single-extractor nom)))))
           
(define yes-no (radio "yes" "no"))

#;(define yes-no
  (λ (nom)
    (gui (λ ()
           (list (radio-input (symbol->string nom) "yes")
                 (radio-input (symbol->string nom) "no")))
         (make-single-extractor nom))))
