launch.sh

```sh
echo "" > tmp
ec2-run-instances ami-4b814f22 -k sat-keypair > tmp

sleep 3
cat tmp | racket rkts/parse-id.rkt > ins.id
cat ins.id
```

rkt/parse-id.rkt

```rkt
(define (in->id in)
   (for ([line (in-lines in)])
      (for ([word (regexp-split #rx"\t+" line)])
         (if (and (string? word)
                  (regexp-match #rx"^i-" word))
             (display word)
             (void)))))
          
(in->id (current-input-port))
```