## Dev tools

```sh
free -m
cat /etc/image-id
cat /etc/system-release
df

sudo yum update
sudo yum groupinstall 'Development Tools'
sudo yum install readline-devel
sudo yum install openssl-devel #POST to https needs libssl.so and libcrypto.so
```

## Apache on Ubuntu (Jaunty)

```sh
aptitude install apache2 apache2.2-common apache2-mpm-prefork apache2-utils libexpat1 ssl-cert

#edit if like
vi /var/www/index.html
# or
sed -i -e 's/It works!/Congratulations FmI!/' /var/www/html/index.html 

## PHP
aptitude install libapache2-mod-php5 php5 php5-common php5-curl php5-dev php5-gd \
php5-imagick php5-mcrypt php5-memcache php5-mhash php5-mysql php5-pspell php5-snmp \
php5-sqlite php5-xmlrpc php5-xsl

cat /etc/apache2/mods-available/php5.load
LoadModule php5_module /usr/lib/apache2/modules/libphp5.so

cat /etc/apache2/mods-available/php5.conf
```

<IfModule mod_php5.c>
  AddType application/x-httpd-php .php .phtml .php3
  AddType application/x-httpd-php-source .phps
</IfModule>

```sh
vi /var/www/test.php
<?php phpinfo(); ?>

chmod 755
a2enmod php5
sudo /etc/init.d/apache2 reload
apache2ctl graceful

## MySQL 

aptitude install mysql-server mysql-client libmysqlclient15-dev

mysqladmin -u root password 'new-password'

mysql -u root -p
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema | 
| mysql              | 
+--------------------+
2 rows in set (0.00 sec)

mysql>use mysql
mysql>quit

mysqladmin -u root -p reload
mysqladmin -u -root -p shutdown
vi /etc/my.cnf
[mysqld]
log-bin
:wq
mysqld-safe
```

## adhoc upload

scp ./up/*.* user@123.45.67.89:/home/user/path/to/
