#lang racket

(require web-server/servlet)
(require web-server/servlet-env);custom start

(require "topsl.rkt")
(require "recorder.rkt") ;permanent save of answers
(require "cache.rkt") ;temporary cache of questions and answers
(require "questions.rkt") ;different types of questions

(define (render-thank-you-page request)
  (response/xexpr
   `(html (head (title "Done")
                (link ((href "/css/fluid_style.css") 
                       (rel "stylesheet")
                       (type "text/css"))) )
          (body (div ((id "fluid-container")) "Thank you")))))

(define (start req)
  
  (define answers-table (make-hash))
  
  (define brands2
    (pull-down "Côte d'Or"
               "Scharffen Burger"
               "Pierre Marcolini"
               "Others"))
  

  (define brands
    (checkbox  "Côte d'Or"
               "Scharffen Burger"
               "Pierre Marcolini"
               "Others"))
               
  ;using syntax defined in topsl
  ;page displays questions and returns mutated hash-table 
  ;from which we extract answer and bound to like-choco?
  ;like-choco? is also used as key in hash-table
  ;? is a question constructor
  (naming ({like-choco?} (page answers-table 
                               (list (? like-choco?  
                                        ("Do you like chocolate?") 
                                        yes-no))))
                                        
          (if (equal? "yes" like-choco?)
              (page answers-table 
                    (list (?  ("Which have you tried?") brands)))
              (page answers-table 
                    (list (?  ("Why do you NOT like chocolate?") free)))) )
  
  (send/suspend/dispatch
   (lambda (embed/url)
     (response/xexpr
      `(html (head (title "Your answer:")
                   (meta ((charset "utf-8")))
                   (link ((href "/css/fluid_style.css") 
                          (rel "stylesheet")
                          (type "text/css"))) )
             (body  (div ((id "fluid-container"))
                
                 ,@(map (λ (q-a)
                                  `(p ,(first q-a)
                                      (br)
                                      ,(if (list? (second q-a))
                                           (string-join (second q-a) ", ")
                                           (second q-a))))
                                (get-all-answers answers-table))
                    (p (a ([href ,(embed/url
                                   (λ (req)
                                     (display (save-data! (get-all-answers answers-table)))
                                     (render-thank-you-page (redirect/get))))])
                          "Save"))))
             ))))
  
  )


(serve/servlet start
               #:command-line? #t
               #:launch-browser? #t
               #:quit? #t
               #:listen-ip #f
               #:port 8080
               #:log-file "log"
               #:extra-files-paths (list (build-path (current-directory) "static"))
               #:servlet-path "/survey")