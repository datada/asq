#lang racket

(require web-server/servlet)
(require "questions.rkt")
(require "cache.rkt")

(provide (all-defined-out))

; 2 macros stolen from TOPSL 

; unamed q syntax is changed to named q syntax
; gensym is too unique for us
(define-syntax (? stx)
  (syntax-case stx ()
    [(id (text ...) gui-builder)
     (let ([nom (string->symbol
                 (symbol->string 
                  (gensym 'unnamed)))])
       #`(id #,nom (text ...) gui-builder))]
    [(id nom (text ...) gui-builder)
     (identifier? #'nom)
     #'(make-q 'nom (list text ...) gui-builder)]))

; map q-name and user answer
(define-syntax (naming stx)
  (define (elt->indentifier elt)
    (syntax-case elt ()
      [(x l) #'x]
      [l #'l]))
  (define (elt->label elt)
    (syntax-case elt ()
      [(x l) #'l]
      [l #'l]))
  (syntax-case stx ()
    [(_ ({elt ...} display-exp-e) body)
     (with-syntax ([(x ...) (map elt->indentifier
                                 (syntax->list #'(elt ...)))]
                   [(l ...) (map elt->label
                                 (syntax->list #'(elt ...)))]
                   [(default ...) (map (λ (x) #'unevaluated)
                                       (syntax->list #'(elt ...)))])
       #'(let ([results display-exp-e])
           ;display-exp-e is (page ...) which returns hash-table
           ;we only want the 'answer' part only
           (let ([x (recall-answer results 'l)] ...)
             body)))]
    [(_ ({elt ...} display-exp-e) body1 body2 ...)
     #'(_ ({elt ...} display-exp-e) (begin body1 body2 ...))]))


(define (page ht qs)
   ;display all questions on one page and wait for the browser to send request
   (define query
     (send/suspend
      (λ (k-url)
        (response/xexpr
         `(html (head (title ,(string-append "Page"))
                      (meta ((charset "utf-8")))
                      (link ((href "/css/fluid_style.css") 
                             (rel "stylesheet")
                             (type "text/css"))) )
                (body (div ((id "fluid-container"))
                 (form ([method "post"]
                        [action
                         ,k-url])

                       ,@(for/list ([q qs])
                                   (question-form q))

                       (br)
                       (input ([type "submit"]
                               [value "Go"]))))))))))

   ;parse user inputs and then update table
   (for ([q qs])
        (let ([answer ((question-parser q) (request-bindings query))])
             (display "before ")
             (display ht)
             (newline)
             (remember-answer! ht q answer)
             (display "after ")
             (display ht)
             (newline)))

   ht)

