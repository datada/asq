Asq.js
======

We want to ask some questions. Based on the answers, we do other things such as asking follow up questions or displaying more information, or saving the answers for later use, etc.

[Live Demo](https://pixbyfunc-hrd.appspot.com/pub/asq/v9-ask.html)

**Short Answer**

+ lib/L1-ask-simple.html

**True False**

+ lib/L2-true-false.html

**Multiple Choice with Follow up**

+ lib/L3-multi-choice-follow-up.html

**Asq DSL (v1)**

+ lib/L4-dsl.html

**Asq DSL (v2 undo via history state)**

+ lib/L5-undo.html



???
======

Can this work with Google Scripts? Turns out, .gs is kinda different ENV/Context. Not clear if worth the effort.

Backend? Firebase hooked up.

reports and charts using d3.js

More forms/shapes?

Q.answerTo("xyz").exclues("one","three");

//likert
Q.ask("Rate the followings.").using("likert low med high", "Food", "Service", "Price");

//pull down using a long list
Q.ask("Favorite char").using("select", ["A", "B", ..., "Z"]);

//validated
Q.ask("What is your email?").using("email required");

Q.ask("What is your phone number?").using("phone");

